## Tutorial Location
   [Chapter Location](https://www.obeythetestinggoat.com/book/chapter_working_incrementally.html)
    
    Working Location in Chapter: Beware of Greedy Regular Expressions

## Code to Execute upon Start Up
```bash
cd "/c/Users/micha/Google_Drive/USU_Classes/Udemy/python-tdd-program"  # Home
cd /h/PythonScripts/git_repositories/test-driven-development # Need to figure out how to delete a virtual environment
```

## NOTES

[How to setup a PATH variable](https://www.youtube.com/watch?v=Y2q_b4ugPWk)

### Updating Packages

```bash
pip install --upgrade <package_name>
```

### Switching Computers
    Make sure to update path to geckodriver in functional tests 
    path. When I finish I will create a file with the paths for 
    the different computers and import it into the module.

### ORM
    An Object-Relational Mapper (ORM) is a layer of abstraction 
    for data stored in a database with tables, rows, and columns. 
    It lets us work with databases using familiar object-oriented 
    metaphors which work well with code. Classes map to database 
    tables, attributes map to columns, and an individual instance 
    of the class represents a row of data in the database.
    
### DATABASE:
    The database is constructed in the models.py file using classes 
    as the tables.

    migrations is actually in charge of building the database. 
    It gives you the ability to add and remove tables and colums, 
    based on changes you make to your models.py files.

    Buidling database migration: $ python manage.py makemigrations
    $ ls lists/migrations

    Classes that inherit from models.Model map to tables in the 
    database. By default they get an auto-generated id attribute, 
    which will be a primary key column in the database, but you 
    have to define any other columns you want explicitly

    PRIMARY KEY: Django has an auto generated primary key. 
    I should see if I can setup my own primary keys for tables.

## LIST TO DO OR WATCH FOR:
    - [x] Don't save blank items for every request
    - [x] Code smell: POST test is too long?
    - [x] Display multiple items in the table
    - [ ] Clean up after FT runs
    - [ ] Support more than one list!

   ### Chapter 7:
    - [x] Adjust model so that items are associated with different lists
    - [x] Add unique URLs for each list
    - [x] Add a URL for creating a new list via POST
    - [ ] Add URLs for adding a new item to an existing list via POST